// document.createElement(tag)
// pElement.insertAdjacentHTML(place, HTMLString); Перший параметр відповідає за місце куди додавати
// "beforebegin" -> before element
// "afterbegin" -> after begin of the element 
// "beforeend" ->  before end of the element
// "afterend" -> after element
// Метод remove та removeChild

const array = [1, 2, 4, "sea", "sun", "34", "21"]; 
function createList(array,parent = document.body){
    const ul = document.createElement("ul");
    document.body.append(ul);
array.forEach((item) => {
    const li = document.createElement("li");
    li.innerText = item;
    ul.appendChild(li);
  });
}
 createList(array, parent);
 

